variable "vpc_id" {
  description = "The VPC to be used for the EFS mount points"
}

variable "environment" {
  description = "The name of the environment (dev,stage, prod, ...)"
}
