resource "aws_efs_file_system" "file_system" {
  tags = {
    Name = "wordpress-${var.environment}-files"
  }
}

data "aws_vpc" "selected" {
  id = "${var.vpc_id}"
}

data "aws_subnet_ids" "subnets" {
  vpc_id = "${data.aws_vpc.selected.id}"
}

resource "aws_efs_mount_target" "mount_point" {
  count = "${length(data.aws_subnet_ids.subnets.ids)}"
  file_system_id = "${aws_efs_file_system.file_system.id}"
  subnet_id = "${element(data.aws_subnet_ids.subnets.ids, count.index)}"
}
