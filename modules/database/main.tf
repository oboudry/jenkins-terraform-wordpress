resource "aws_db_instance" "mysqldb" {
  engine              = "mysql"
  allocated_storage   = "${var.db_storage}"
  instance_class      = "${var.db_instance_type}"
  name                = "${var.environment}_db"
  username            = "${var.db_user}"
  password            = "${var.db_password}"
  skip_final_snapshot = true
}
