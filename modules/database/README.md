# MySQL RDS Database

This folder contains a [Terraform](https://www.terraform.io/) module that can be used to deploy an AWS RDS MySQL database.

## How do you use this module?

This folder defines a [Terraform module](https://www.terraform.io/docs/modules/usage.html), which you can use in your
code by adding a `module` configuration and setting its `source` parameter to URL of this folder:

```hcl
module "rds_mysql_database" {
  TODO example code here ...
}
```
