variable "db_instance_type" {
  description = "The instance type for the database"
  default = "db.t2.micro"
}

variable "db_storage" {
  description = "The size of the DB drive in GB"
  default = 8
}

variable "db_user" {
  description = "The user for the database"
  default = "admin"
}

variable "db_password" {
  description = "The password for the database"
  default = "password"
}

variable "environment" {
  description = "The environment"
  default = "dev"
}
