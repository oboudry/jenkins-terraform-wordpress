provider "aws" {
  region = "eu-central-1"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket  = "oboudry-terraform-state"
    dynamodb_table = "terraform-state-lock-dynamo"
    region  = "us-east-1"
    # -backend-config="key=wordpress-dev"
  }
}

module "database" {
  source              = "modules/database"
  db_storage          = 8
  db_instance_type    = "db.t2.micro"
  db_user             = "admin"
  db_password         = "password"
  environment         = "dev"
}

module "efs" {
  source              = "modules/file_system"
  vpc_id              = "vpc-aebfccc5"
  environment         = "dev"
}
